﻿using homework_extracredit.View;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace homework_extracredit
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new LandingPage()); //setting main page as navigation page
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
